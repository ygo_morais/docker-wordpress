# DOCKER-WORDPRESS

Projeto base para ser utilizado para criação de novos projetos com Wordpress com Docker.
Já vem integrado o SGBD, MySql e o PhpMyAdmin, para ajudar a gerenciar o banco de dados.

## Requisitos do ambiente de desenvolvimento/produção

- Docker
- Docker-compose

## Micro serviços disponíveis

- wordpress:5.8.1
- mysql:5.7.35
- phpmyadmin: 5.1.1

## Criando novo projeto

Baixe o projeto base:

```bash
git clone https://bitbucket.org/ygo_morais/docker-wordpress.git
```

Renomeie o repositório para o nome do seu projeto:

```bash
mv docker-wordpress/ <nome_projeto>
```

Acesse o seu projeto:

```bash
cd <nome_projeto>
```

Desvincule o versionamento ligado a esse projeto:

```bash
rm -rf .git
```

Crie o arquivo de credencias da base de dados e outras informações:

```bash
cp .env.example .env
```

Agora abra o arquivo _.env_ e configure as credencias do banco de dados e portas de acesso. Lembre-se que as portas configuradas nesse arquivo, devem está disponíveis para que possa evitar erros como: `Error starting userland proxy: listen tcp 0.0.0.0:8080: bind: address already in use`.

Ao executar os comandos abaixo, espere por alguns segundo (pelo menos 30 segundos), para a criação da base de dados. Dica: Se visualizar a linha `socket: '/var/run/mysqld/mysqld.sock' port: 3306 MySQL Community Server (GPL)`, no log da criação dos containers, o projeto está pronto para ser utilizado).

- Ambiente de desenvolvimento (visualizando logs):

```bash
./start-dev.sh
```

- Ambiente de produção (modo background):

```bash
./start-prod.sh
```

- Parando o container:

```bash
./stop.sh
```

## Acesso aos serviços

As portas estão configuradas no arquivo _.env_.

### Wordpress

<http://localhost:[WP_PORTA]>

### Phpmyadmin

<http://localhost:[PHPMYADMIN_PORTA]>
